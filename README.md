![Build Status](https://gitlab.com/spookyblackbox/kvtool/badges/master/build.svg)

Live page: [here](https://spookyblackbox.gitlab.io/kvtool)

## Toolbar

'input files'
- opens file selection panel
- source and destination files are newline delimited
- processing script is a js file (see process-script-template.js)


'text output'
- provides a formated string of your current dest:source mappings.
- String is returned as JSON unless processing script is provided


'reset destinations'
- clears source:destination mappings

'reset all'
- clears all page data, including files and processing script


'light bulb'
- toggle light/dark theme


## Input
ctrl + click in the source panel 
- will add toggle a source in your current selection

ctrl + z
- undo your previous action (file loads, source:dest assignemnts, etc)

click + drag on a source
- drag source(s) to destination to assign


click on the red x to remove a source from its destination

Current app state is retained on chrome/firefox.
