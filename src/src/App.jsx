function deepCopy(obj) {
	try {
		return JSON.parse(JSON.stringify(obj));
	} catch(e) {
		console.error('Deep copy failed, object likely contained circular references');
		return undefined;
	}
}

let app = {
	_ : {
		_defaultState: {
			sourceFileName: 'no file',
			destinationFileName: 'no file',
			scriptFileName: 'no file',
			sources: [],
			destinations: [],
			mapping: [],
			hovered: null,
			selected: [],
			destMap: {},
			pipeSource: '',
			outputString: '',
		}
	},

	appRoot: null,
	keyState : {
		ctrl: false,
		alt: false,
		shift: false,
	},


	state: {
		sourceFileName: 'no file',
		destinationFileName: 'no file',
		scriptFileName: 'no file',
		sources: [],
		destinations: [],
		mapping: [],
		hovered: null,
		selected: [],
		destMap: {},
		pipeSource: '',
		outputString: '',
	},
	history: [],


	resetState : () =>{
		app.setState(deepCopy(app._._defaultState));
	},

	getState : (key) =>{
		if(typeof(app.state[key]) != undefined)
			return deepCopy(app.state[key]);

		return undefined;
	},

	setState : (obj, commit = true, redraw = true) =>{
		let stateCopy = deepCopy(app.state);

		let changes = 0;
		Object.keys(obj).forEach(key =>{
			if(key in app.state && app.state[key] == obj[key])
				return;

			app.state[key] = obj[key];
			changes += 1;
		});
		
		if(commit && changes != 0){
			app.history.unshift(stateCopy);
			if(app.history.length > 16)
				app.history.splice(15, 1);

			app.save();

		}

		if(redraw)
			app.redraw();
	},

	undo : () =>{
		if(app.history.length > 0) {
			let state = deepCopy(app.history[0]);
			if(state != undefined){
				app.state = state;
				app.history.splice(0, 1);
			}
		}

		app.redraw();
	},

	redraw : () =>{
		if(app.appRoot != null)
			app.appRoot.forceUpdate();
	},

	save : () =>{
		if(typeof(Storage) != "undefined")
			localStorage.setItem("app", JSON.stringify(app.state));
	},

	load : () =>{
		if(typeof(Storage) != "undefined"){
			let r = JSON.parse(localStorage.getItem("app"));

			if(r != undefined && r != null)
				app.state = r;
		}
	},

	mapToText : () =>{
		let pipeSource = app.getState('pipeSource');
		if(!pipeSource || pipeSource.length == 0)
			return JSON.stringify(app.getState('destMap'));

		try {
			console.log('parsing');
			let pipe
			try{
				pipe = eval(pipeSource);
			}catch(e){
				throw "E_INVALID_SOURCE";
			}

			if(pipe == undefined)
				throw('E_BAD_SOURCE');

			pipe = pipe();

			if(   !('before' in pipe)
				|| !('after' in pipe)
				|| !('pipeOverride' in pipe)
				|| !('procDest' in pipe)
				|| !('override_pipe' in pipe)
			) {
				throw('INVALID PROCESS SCRIPT');
			}


			let map = app.getState('destMap');
			let r = '';
			if(!pipe.override_pipe) {
				r = pipe.before(r);

				Object.keys(map).forEach(dest =>{
					r = pipe.procDest(dest, map[dest], r);
				});

				r = pipe.after(r);
			}

			else {
				r = pipe.pipeOverride(map);
			}

			return r;

		}
		catch(e){
			return JSON.stringify(e);
		}

		return 'something went wrong';
	},

	loadPipeCode : (e) =>{
		let f = e.target.files[0];

		let reader = new FileReader();
		reader.onload = () =>{
			app.setState({scriptFileName: f.name, pipeSource: reader.result}, true, true);
		};

		reader.readAsText(f);
	},

	loadKeyFile : (type, e) =>{
		let f = e.target.files[0];

		if(!f) return;
		let key = type;
		let fileNameKey = 'sourceFileName';
		if(key == 'destinations')
			fileNameKey = 'destinationFileName';

		let reader = new FileReader();
		reader.onload = () =>{
			let r = {};
			r[key] = app.parseKeyString(reader.result)
			r['selected'] = [];
			r[fileNameKey] = f.name;

			app.setState(r, false, true);
			app.initDestMap();

			app.setState(r, true, true);
		}

		reader.readAsText(f);		
	},

	parseKeyString : (str) =>{
		let r = str.split('\n');

		for(let i = 0; i < r.length; i++)
			r[i] = r[i].replace(/\r?\n|\r/, '');

		return r;
	},

	initDestMap : () =>{
		if(
			   !app.state.destinations
			|| app.state.destinations.length == 0
		)
			return;

		let destMap = {};

		app.state.destinations.forEach(dest =>{
			destMap[dest] = [];
		});

		app.setState({destMap : destMap});
	},

	addSources : (dest, sources) =>{
		let dMap = deepCopy(app.state.destMap);

		if(!dest in dMap) return;

		sources.forEach(src =>{
			if(dMap[dest].includes(src)) return;
			dMap[dest].push(src);
		});

		app.setState({destMap: dMap});
	},

	removeSources : (dest, sources) =>{
		let dMap = deepCopy(app.state.destMap);

		if(!dest in dMap) return;

		sources.forEach(src =>{
			let idx = dMap[dest].indexOf(src);
			if(idx == -1) return;

			dMap[dest].splice(idx, 1);
		});

		app.setState({destMap: dMap});
	},

	toggleSelection : (source) =>{
		
		let selection = deepCopy(app.state.selected);
		let idx = selection.indexOf(source);
		
		if(idx == -1)
			selection.push(source);

		else
			selection.splice(idx, 1);

		app.setState({selected: selection}, false, true);

		return;
	},

	saveMapToText : () => {
		let str = app.mapToText();
		let blob = new Blob([str], {type: 'text/plain;charset=utf-8'});

		saveAs(blob, 'result.txt');
	},

	saveState : () =>{

		let str = JSON.stringify(app.state);
		let blob = new Blob([str], {type: 'text/plain;charset=utf-8'});

		saveAs(blob, 'mappings.sd.json');

	},

	loadSerializedState : (e) =>{
		let f = e.target.files[0];

		if(!f) return;

		let reader = new FileReader();
		reader.onload = () =>{
			let r = JSON.parse(reader.result);

			if(!r) return;

			app.state = r;

			app.redraw();
		}

		reader.readAsText(f);		
	},


	setSelection : (source) =>{
		
		app.setState({selected: [source]}, false, true);
	},

	keyUpHandler : (e) =>{
		
		app.keyState.ctrl = e.ctrlKey;

		if(e.key == 'z' && app.keyState.ctrl)
			app.undo();
	},

	keyDownHandler : (e) =>{
		
		app.keyState.ctrl = e.ctrlKey;
	},

	init : () =>{
		document.body.addEventListener('keyup', app.keyUpHandler);
		document.body.addEventListener('keydown', app.keyDownHandler);

		window.onresize = app.redraw();
	}
}

app.init();

const colors = {

	core: {
		blue: '#44e',
		purple: '#a4a'
	},
	light: {
		background: '#242426',
		bgContrast: '#eee',

		text: '#eee',
		textContrast: '#000',

	},

	dark: {
		background: '#242426',
		bgContrast: '#242426',

		text: '#eee',
		textContrast: '#eee',

	}
};

Object.assign(colors.dark, colors.core);
Object.assign(colors.light, colors.core);

let theme = colors.light;
//theme = colors.light;

class ToolBar extends React.Component {
	constructor(props) {
		super(props);

		this.renderStyle();

		this.state = {
			inputFile : '',
			outputFile : '',
		}
	}
	
	renderStyle(){
		this.styles = {
			header: {
				width: '100%',
				boxSizing: 'border-box',
				minHeight: 32,
				background: theme.bgContrast,
				padding: 4,
				color: theme.textContrast
			}
		}
	}

	render() {

		this.renderStyle();

		return(
			<header style={this.styles.header}>
				<button
					onClick={this.props.setFileModalVisible}
				>
					input files
				</button>
				<button
					onClick={this.props.setOutputModalVisible}
				>
					text output
				</button>
				<button
					onClick={app.saveState}
				>
					save state
				</button>

				<button 
					onClick={this.props.toggleDarkMode}
					style={{float: 'right', background: 'none', border:'none'}}
				>
					💡
				</button>
				

				<button
					style={{float: 'right'}}
					onClick={app.resetState}
				>
					reset all
				</button>

				<button
					style={{float: 'right'}}
					onClick={app.initDestMap}
				>
					reset destinations
				</button>

			</header>
		);
	}
}

class AppRoot extends React.Component {
	constructor(props){ 
		super(props);
		app.load();
		app.appRoot = this;

		this.state = {
			fileModalVisible: false,
			outputModalVisible: false,
		}
	}

	renderStyle(){
		this.style = {
			display: 'flex',
			flexDirection : 'column',
			width: '100vw',
			height: '100vh',
			background: theme.background,
			color: theme.text,
			overflow: 'hidden',
		};
	}

	toggleDarkMode() {
		if(theme == colors.light)
			theme = colors.dark;
		else
			theme = colors.light;

		this.forceUpdate();
	}

	setFileModalVisible(visible) {
		this.setState({fileModalVisible: visible});
	}

	setOutputModalVisible(visible) {
		this.setState({outputModalVisible: visible});
	}
	
	render() {
		this.renderStyle();
		return(
			<div style={this.style}>
				<div style={Object.assign({}, this.style, {})}>
					<ToolBar 
						toggleDarkMode={this.toggleDarkMode.bind(this)}
						setFileModalVisible={(() =>this.setFileModalVisible(true))}
						setOutputModalVisible={(() =>this.setOutputModalVisible(true))}
					></ToolBar>
					<div style={{flex: '1 1', width: '100%', height: '100%', display: 'flex', flexDirection: 'row', flexWrap: 'wrap',}}>
						<SourcePalette></SourcePalette>
						<DestinationFields></DestinationFields>
					</div>
				</div>
				<FileModal setHidden={(() =>this.setFileModalVisible(false))} visible={this.state.fileModalVisible}></FileModal>
				<OutputModal setHidden={(() =>this.setOutputModalVisible(false))} visible={this.state.outputModalVisible}></OutputModal>

			</div>
		);
	}
}

class SourcePalette extends React.Component {
	constructor(props) {
		super(props);
		this.renderStyle();

		this.state = {
			filter : ''
		};
	}

	renderStyle() {
		this.styles = {
			container : {
				display: 'flex',
				flex: '1 2 180px',
				flexDirection: 'column',
				height: '100%',
				width: 180,
				border: '1px solid ' + theme.blue
			},
			body: {
				boxSizing: 'border-box',
				padding: '6px 0',
				flex: '1 1 auto',
				width: '100%', height: 0,
				overflowY: 'auto'
			},
			source : {
				display:'inline-block',
				boxSizing: 'border-box',
				float: 'left',
				width: '100%',
				height: 32,
				lineHeight: '32px',
				cursor: 'default',
				padding: '0 6px'
			},
			searchBox : {
				height: 32, width: '100%',
				border:'none',
				borderBottom: '1px solid ' + theme.blue,
				background: theme.background,
				color: theme.text,
				boxSizing: 'border-box',
				display: 'inline-block',
				fontSize: '1rem',
				fontFamily: 'inherit'
			},
		}
	}

	renderSourceElements(half_width = false) {

		console.log(app.state.sources.length);

		let r = [];
		let mul = 1.0;
		if (half_width) {
			mul = 0.5;
		}


		app.state.sources.forEach(source =>{

			if(
				this.state.filter.length > 0
				&& !(source.indexOf(this.state.filter) == 0)
			)
				return;


			let style = this.styles.source;
			style = Object.assign({}, style, {width: (100 * mul) + '%'});
			if(app.getState('selected').indexOf(source) != -1){
				
				style = Object.assign({}, style, {background: theme.blue});
			} 

			else if(false && app.getState('hovered') == source) {

				style = Object.assign({}, style, {boxShadow: 'inset 0 0 24px -16px ' + theme.blue});
			}

			r.push(
				<span 
					draggable
					title={source}
					onMouseEnter={() =>{
						//app.setState({hovered: source}, false, true);
					}}
					onMouseLeave={() =>{
						//app.setState({hovered: null}, false, true);
					}}
					onMouseUp={(e) =>{
						if(e.ctrlKey){
							
							app.toggleSelection(source);
							return;
						}

						app.setSelection(source);
					}}
					onDragStart={(e) =>{ 
						if(!app.state.selected.includes(source)){
							app.setSelection(source);
						}

						e.dataTransfer.setData('application/json', JSON.stringify(app.state.selected));
						e.dataTransfer.effectAllowed = 'move';
					}}
					style={style}
				>
						{source}
				</span>
			);
		});
		return r;
	}

	render() {
		this.renderStyle();

		this.styles.container.size = 180;
		let half = false;

		if(
			(56+(app.getState('sources').length * 32)) / window.innerHeight > 1.0
			&& window.innerWidth > 640
		){
			this.styles.container = Object.assign(this.styles.container, {width: 360, flex: '1 2 360px'});
			half = true;
		}

		else
			this.styles.container = Object.assign(this.styles.container, {width: 180, flex: '1 2 180px'});


		return (
			<div style={this.styles.container}>
				<div style={this.styles.body}>
					<input 
						type="text"
						placeholder="filter"
						onChange={(e) => this.setState({filter: e.target.value})}
						value={this.state.filter}
						style={this.styles.searchBox}
					/>
					{this.renderSourceElements(half)}
				</div>
			</div>
		);
	}
}

class DestinationFields extends React.Component {
	constructor(props) {
		super(props);
		this.renderStyle();
	}


	renderStyle() {
		this.styles = {
			container : {
				display: 'flex',
				flex: '300 1 240px',
				flexDirection: 'column',
				height: '100%',
				width: 240,
				border: '1px solid ' + theme.purple
			},
			body : {
				boxSizing: 'border-box',
				padding: 6,
				width: '100%',
				height: 0,
				flex: '1 1 auto',
				display: 'flex', flexDirection: 'row',
				flexWrap: 'wrap', flexSpacing: 'start',
			},
			destination : {
				boxSizing: 'border-box',
				width: 240, minHeight: 80,
				padding: 6,
				flex: '1 0 240px',
				background: theme.bgContrast,
				color: theme.textContrast,
				border: '1px solid ' + theme.purple,
			}
		}
	}

	renderDestSources(dest) {
		let r = [];

		let sources = app.getState('destMap')[dest];
		if(!sources)
			return;

		let idx = 0;
		sources.forEach(src =>{
			r.push ( 
				<li key={JSON.stringify({src:src, idx:idx})}>
					<span>{src}</span>
					<button
						style={{
							background: 'none',
							border: 'none',
							margin: 0,
							color: '#f00'
						}}
						onClick={()=>{
							app.removeSources(dest, [src]);
						}}
					>X</button>
				</li>
			);
		});

		return r;
	}

	renderDestinations() {
		let r = [];

		app.state.destinations.forEach(dest =>{

			let style = this.styles.destination;
			if(
				false &&
				app.getState('destMap')[dest].includes(app.getState('hovered'))
			) {
				style = Object.assign({}, style, {boxShadow: 'inset 0 0 24px -8px ' + theme.purple + ', 0 0 16px -8px ' + theme.purple})
			}

			r.push(
				<div
					onDragOver={(e) =>{e.preventDefault();}} 
					onDrop={(e) =>{e.preventDefault(); app.addSources(dest, JSON.parse(e.dataTransfer.getData('application/json')));}} 
					style={style}
					key={dest + r.length}
					>
					<b>{dest}</b>
					<ul>
						{this.renderDestSources(dest)}
					</ul>
				</div>
			);
		});

		return r;
	}

	render() {
		this.renderStyle();

		return (
			<div style={this.styles.container}>
				<div style={{width: '100%', maxHeight: '100%', height: '100%', display: 'inline-block', overflowY: 'scroll'}}>
					<div style={this.styles.body}>
						{this.renderDestinations()}
					</div>
				</div>
			</div>
		);
	}
}

class FileModal extends React.Component {
	constructor(props) {

		super(props);
	}

	render() {
		let display = 'inline-block';
		if(!this.props.visible){
			display = 'none';

		}

		return(

			<div style={{
				display: display,
				position: 'fixed', zIndex: 12, 
				top: 0, left: 0, bottom: 0, right: 0,
				background: 'rgba(128, 128, 128, .5)',
			}}
			>
				<div style={{
						width: 640, height: 400, 
						background: theme.bgContrast,
						position: 'absolute',
						left: '50%', top: '50%',
						transform: 'translate(-50%, -50%)',
						border: '2px solid ' + theme.background,
						color: theme.textContrast,
						padding: 12
					}}
				>
					<button
						style={{position: 'absolute', right: 0, top: 0}}
						onClick={this.props.setHidden}
					>
						close
					</button>
					<span>sources: </span>
					<input 
						type='file'
						accept='text'
						onChange={(e) =>{
							app.loadKeyFile('sources', e);
							e.target.value = null;
						}}
						/*
						onChange={(e) =>{
							app.loadKeyFile('sources', e);
							e.target.value = null;
						}}*/
					/>
					<span>{app.getState('sourceFileName')}</span>
					<br />
					<br />
					<span>destinations: </span>
					<input 
						type='file'
						accept='text'
						onChange={
							(e) =>{app.loadKeyFile('destinations', e);
							e.target.value = null;
						}}
					/>
					<span>{app.getState('destinationFileName')}</span>
					<br />
					<br />
					<span>processing script: </span>
					<input 
						type='file'
						accept='text/javascript'
						onChange={(e) =>{
							app.loadPipeCode(e);
							e.target.value = null;
						}}
					/>
					<span>{app.getState('scriptFileName')}</span>
					<br />
					<br />
					<br />
					<span>load previous state: </span>
					<input 
						type='file'
						accept='text/javascript'
						onChange={(e) =>{
							app.loadSerializedState(e);
							e.target.value = null;
						}}
					/>
				</div>
			</div>
		);
	}
}
class OutputModal extends React.Component {
	constructor(props) {

		super(props);
	}

	render() {
		let display = 'inline-block';
		if(!this.props.visible){
			return <div />;
		}

		return(

			<div style={{
				display: display,
				boxSizing: 'border-box',
				position: 'fixed', zIndex: 12, 
				top: 0, left: 0, bottom: 0, right: 0,
				background: 'rgba(128, 128, 128, .5)',
				padding: 32
			}}
			>
				<div style={{
						background: theme.background,
						position: 'relative',
						height: '100%', width: '100%',
						boxSizing: 'border-box',
						border: '2px solid ' + theme.bgContrast,
						color: theme.text,
						padding: 12
					}}
				>
					<button
						style={{
							position: 'absolute',
							left: 0, top: 0,
						}}
						onClick={app.saveMapToText}
					>
						save output
					</button>
					<button
						style={{position: 'absolute', right: 0, top: 0}}
						onClick={this.props.setHidden}
					>
						close
					</button>
					<p
						contentEditable='true'
						style={{
							position: 'absolute',
							left: 12, right: 12, top: 16, bottom: 0,
							display: 'inline-block',
							boxSizing: 'border-box',
							overflowY: 'scroll',
							whiteSpace: 'pre-wrap',
							padding: 12,
							background: theme.bgContrast,
							color: theme.textContrast
						}}
					>
						{app.mapToText() || 'something went wrong'}
					</p>
				</div>
			</div>
		);
	}
}


function Application() {
	return (
		<AppRoot>
		</AppRoot>
	);
}

export default Application;
