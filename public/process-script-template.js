
/////////////////////////////////////
//pipeline
/////////////////////////////////////
//Executed before destination:source mappings are processed
function before (str) {
	str = 'These are the s:d mappings:\n';
	return str;
}


//foreach destination. sources is an array of strings
function procDest(destination, sources, str) {
	
	str += destination + ': [ ';
	sources.forEach(src => str += src + ' ');
	str += ']\n';

	return str;
}


//Executed after mappings are processed
function after (str) {

	str += '\nthat\'s all, folks.';
	
	return str;
}


//executed instead of the pipe.
function pipeOverride(mappings) {
	let str = 'pipe override: \n';

	Object.keys(mappings).forEach(dest =>{
		str += dest + ' : ';
		mappings[dest].forEach(source =>{
			str += source + ' '
		});

		str += '\n\n';
	});

	str += '\nthat\'s all, folks.';
	
	return str;
}

() =>{return {
	before: before,
	procDest: procDest,
	after: after,
	override_pipe: false, //set true if you're using pipe override
	pipeOverride: pipeOverride
}};