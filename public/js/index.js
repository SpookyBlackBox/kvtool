"use strict";

document.addEventListener("DOMContentLoaded", () =>{
	ReactDOM.render(
	  React.createElement(AppRoot, null),
	  document.getElementById("root")
	);}); // If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
