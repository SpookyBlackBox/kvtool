"use strict";

function _instanceof(left, right) {
  if (
    right != null &&
    typeof Symbol !== "undefined" &&
    right[Symbol.hasInstance]
  ) {
    return !!right[Symbol.hasInstance](left);
  } else {
    return left instanceof right;
  }
}

function _classCallCheck(instance, Constructor) {
  if (!_instanceof(instance, Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }
  return _assertThisInitialized(self);
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError(
      "this hasn't been initialised - super() hasn't been called"
    );
  }
  return self;
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf
    ? Object.getPrototypeOf
    : function _getPrototypeOf(o) {
        return o.__proto__ || Object.getPrototypeOf(o);
      };
  return _getPrototypeOf(o);
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }
  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: { value: subClass, writable: true, configurable: true }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf =
    Object.setPrototypeOf ||
    function _setPrototypeOf(o, p) {
      o.__proto__ = p;
      return o;
    };
  return _setPrototypeOf(o, p);
}

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    _typeof = function _typeof(obj) {
      return obj &&
        typeof Symbol === "function" &&
        obj.constructor === Symbol &&
        obj !== Symbol.prototype
        ? "symbol"
        : typeof obj;
    };
  }
  return _typeof(obj);
}

function deepCopy(obj) {
  try {
    return JSON.parse(JSON.stringify(obj));
  } catch (e) {
    console.error(
      "Deep copy failed, object likely contained circular references"
    );
    return undefined;
  }
}

var app = {
  _: {
    _defaultState: {
      sourceFileName: "no file",
      destinationFileName: "no file",
      scriptFileName: "no file",
      sources: [],
      destinations: [],
      mapping: [],
      hovered: null,
      selected: [],
      destMap: {},
      pipeSource: "",
      outputString: ""
    }
  },
  appRoot: null,
  keyState: {
    ctrl: false,
    alt: false,
    shift: false
  },
  state: {
    sourceFileName: "no file",
    destinationFileName: "no file",
    scriptFileName: "no file",
    sources: [],
    destinations: [],
    mapping: [],
    hovered: null,
    selected: [],
    destMap: {},
    pipeSource: "",
    outputString: ""
  },
  history: [],
  resetState: function resetState() {
    app.setState(deepCopy(app._._defaultState));
  },
  getState: function getState(key) {
    if (_typeof(app.state[key]) != undefined) return deepCopy(app.state[key]);
    return undefined;
  },
  setState: function setState(obj) {
    var commit =
      arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
    var redraw =
      arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
    var stateCopy = deepCopy(app.state);
    var changes = 0;
    Object.keys(obj).forEach(function(key) {
      if (key in app.state && app.state[key] == obj[key]) return;
      app.state[key] = obj[key];
      changes += 1;
    });

    if (commit && changes != 0) {
      app.history.unshift(stateCopy);
      if (app.history.length > 16) app.history.splice(15, 1);
      app.save();
    }

    if (redraw) app.redraw();
  },
  undo: function undo() {
    if (app.history.length > 0) {
      var state = deepCopy(app.history[0]);

      if (state != undefined) {
        app.state = state;
        app.history.splice(0, 1);
      }
    }

    app.redraw();
  },
  redraw: function redraw() {
    if (app.appRoot != null) app.appRoot.forceUpdate();
  },
  save: function save() {
    if (typeof Storage != "undefined")
      localStorage.setItem("app", JSON.stringify(app.state));
  },
  load: function load() {
    if (typeof Storage != "undefined") {
      var r = JSON.parse(localStorage.getItem("app"));
      if (r != undefined && r != null) app.state = r;
    }
  },
  mapToText: function mapToText() {
    var pipeSource = app.getState("pipeSource");
    if (!pipeSource || pipeSource.length == 0)
      return JSON.stringify(app.getState("destMap"));

    try {
      console.log("parsing");
      var pipe;

      try {
        pipe = eval(pipeSource);
      } catch (e) {
        throw "E_INVALID_SOURCE";
      }

      if (pipe == undefined) throw "E_BAD_SOURCE";
      pipe = pipe();

      if (
        !("before" in pipe) ||
        !("after" in pipe) ||
        !("pipeOverride" in pipe) ||
        !("procDest" in pipe) ||
        !("override_pipe" in pipe)
      ) {
        throw "INVALID PROCESS SCRIPT";
      }

      var map = app.getState("destMap");
      var r = "";

      if (!pipe.override_pipe) {
        r = pipe.before(r);
        Object.keys(map).forEach(function(dest) {
          r = pipe.procDest(dest, map[dest], r);
        });
        r = pipe.after(r);
      } else {
        r = pipe.pipeOverride(map);
      }

      return r;
    } catch (e) {
      return JSON.stringify(e);
    }

    return "something went wrong";
  },
  loadPipeCode: function loadPipeCode(e) {
    var f = e.target.files[0];
    var reader = new FileReader();

    reader.onload = function() {
      app.setState(
        {
          scriptFileName: f.name,
          pipeSource: reader.result
        },
        true,
        true
      );
    };

    reader.readAsText(f);
  },
  loadKeyFile: function loadKeyFile(type, e) {
    var f = e.target.files[0];
    if (!f) return;
    var key = type;
    var fileNameKey = "sourceFileName";
    if (key == "destinations") fileNameKey = "destinationFileName";
    var reader = new FileReader();

    reader.onload = function() {
      var r = {};
      r[key] = app.parseKeyString(reader.result);
      r["selected"] = [];
      r[fileNameKey] = f.name;
      app.setState(r, false, true);
      app.initDestMap();
      app.setState(r, true, true);
    };

    reader.readAsText(f);
  },
  parseKeyString: function parseKeyString(str) {
    var r = str.split("\n");

    for (var i = 0; i < r.length; i++) {
      r[i] = r[i].replace(/\r?\n|\r/, "");
    }

    return r;
  },
  initDestMap: function initDestMap() {
    if (!app.state.destinations || app.state.destinations.length == 0) return;
    var destMap = {};
    app.state.destinations.forEach(function(dest) {
      destMap[dest] = [];
    });
    app.setState({
      destMap: destMap
    });
  },
  addSources: function addSources(dest, sources) {
    var dMap = deepCopy(app.state.destMap);
    if (!dest in dMap) return;
    sources.forEach(function(src) {
      if (dMap[dest].includes(src)) return;
      dMap[dest].push(src);
    });
    app.setState({
      destMap: dMap
    });
  },
  removeSources: function removeSources(dest, sources) {
    var dMap = deepCopy(app.state.destMap);
    if (!dest in dMap) return;
    sources.forEach(function(src) {
      var idx = dMap[dest].indexOf(src);
      if (idx == -1) return;
      dMap[dest].splice(idx, 1);
    });
    app.setState({
      destMap: dMap
    });
  },
  toggleSelection: function toggleSelection(source) {
    var selection = deepCopy(app.state.selected);
    var idx = selection.indexOf(source);
    if (idx == -1) selection.push(source);
    else selection.splice(idx, 1);
    app.setState(
      {
        selected: selection
      },
      false,
      true
    );
    return;
  },
  saveMapToText: function saveMapToText() {
    var str = app.mapToText();
    var blob = new Blob([str], {
      type: "text/plain;charset=utf-8"
    });
    saveAs(blob, "result.txt");
  },
  saveState: function saveState() {
    var str = JSON.stringify(app.state);
    var blob = new Blob([str], {
      type: "text/plain;charset=utf-8"
    });
    saveAs(blob, "mappings.sd.json");
  },
  loadSerializedState: function loadSerializedState(e) {
    var f = e.target.files[0];
    if (!f) return;
    var reader = new FileReader();

    reader.onload = function() {
      var r = JSON.parse(reader.result);
      if (!r) return;
      app.state = r;
      app.redraw();
    };

    reader.readAsText(f);
  },
  setSelection: function setSelection(source) {
    app.setState(
      {
        selected: [source]
      },
      false,
      true
    );
  },
  keyUpHandler: function keyUpHandler(e) {
    app.keyState.ctrl = e.ctrlKey;
    if (e.key == "z" && app.keyState.ctrl) app.undo();
  },
  keyDownHandler: function keyDownHandler(e) {
    app.keyState.ctrl = e.ctrlKey;
  },
  init: function init() {
    document.body.addEventListener("keyup", app.keyUpHandler);
    document.body.addEventListener("keydown", app.keyDownHandler);
    window.onresize = app.redraw();
  }
};
app.init();
var colors = {
  core: {
    blue: "#44e",
    purple: "#a4a"
  },
  light: {
    background: "#242426",
    bgContrast: "#eee",
    text: "#eee",
    textContrast: "#000"
  },
  dark: {
    background: "#242426",
    bgContrast: "#242426",
    text: "#eee",
    textContrast: "#eee"
  }
};
Object.assign(colors.dark, colors.core);
Object.assign(colors.light, colors.core);
var theme = colors.light; //theme = colors.light;

var ToolBar =
  /*#__PURE__*/
  (function(_React$Component) {
    _inherits(ToolBar, _React$Component);

    function ToolBar(props) {
      var _this;

      _classCallCheck(this, ToolBar);

      _this = _possibleConstructorReturn(
        this,
        _getPrototypeOf(ToolBar).call(this, props)
      );

      _this.renderStyle();

      _this.state = {
        inputFile: "",
        outputFile: ""
      };
      return _this;
    }

    _createClass(ToolBar, [
      {
        key: "renderStyle",
        value: function renderStyle() {
          this.styles = {
            header: {
              width: "100%",
              boxSizing: "border-box",
              minHeight: 32,
              background: theme.bgContrast,
              padding: 4,
              color: theme.textContrast
            }
          };
        }
      },
      {
        key: "render",
        value: function render() {
          this.renderStyle();
          return React.createElement(
            "header",
            {
              style: this.styles.header
            },
            React.createElement(
              "button",
              {
                onClick: this.props.setFileModalVisible
              },
              "input files"
            ),
            React.createElement(
              "button",
              {
                onClick: this.props.setOutputModalVisible
              },
              "text output"
            ),
            React.createElement(
              "button",
              {
                onClick: app.saveState
              },
              "save state"
            ),
            React.createElement(
              "button",
              {
                onClick: this.props.toggleDarkMode,
                style: {
                  float: "right",
                  background: "none",
                  border: "none"
                }
              },
              "\uD83D\uDCA1"
            ),
            React.createElement(
              "button",
              {
                style: {
                  float: "right"
                },
                onClick: app.resetState
              },
              "reset all"
            ),
            React.createElement(
              "button",
              {
                style: {
                  float: "right"
                },
                onClick: app.initDestMap
              },
              "reset destinations"
            )
          );
        }
      }
    ]);

    return ToolBar;
  })(React.Component);

var AppRoot =
  /*#__PURE__*/
  (function(_React$Component2) {
    _inherits(AppRoot, _React$Component2);

    function AppRoot(props) {
      var _this2;

      _classCallCheck(this, AppRoot);

      _this2 = _possibleConstructorReturn(
        this,
        _getPrototypeOf(AppRoot).call(this, props)
      );
      app.load();
      app.appRoot = _assertThisInitialized(_this2);
      _this2.state = {
        fileModalVisible: false,
        outputModalVisible: false
      };
      return _this2;
    }

    _createClass(AppRoot, [
      {
        key: "renderStyle",
        value: function renderStyle() {
          this.style = {
            display: "flex",
            flexDirection: "column",
            width: "100vw",
            height: "100vh",
            background: theme.background,
            color: theme.text,
            overflow: "hidden"
          };
        }
      },
      {
        key: "toggleDarkMode",
        value: function toggleDarkMode() {
          if (theme == colors.light) theme = colors.dark;
          else theme = colors.light;
          this.forceUpdate();
        }
      },
      {
        key: "setFileModalVisible",
        value: function setFileModalVisible(visible) {
          this.setState({
            fileModalVisible: visible
          });
        }
      },
      {
        key: "setOutputModalVisible",
        value: function setOutputModalVisible(visible) {
          this.setState({
            outputModalVisible: visible
          });
        }
      },
      {
        key: "render",
        value: function render() {
          var _this3 = this;

          this.renderStyle();
          return React.createElement(
            "div",
            {
              style: this.style
            },
            React.createElement(
              "div",
              {
                style: Object.assign({}, this.style, {})
              },
              React.createElement(ToolBar, {
                toggleDarkMode: this.toggleDarkMode.bind(this),
                setFileModalVisible: function setFileModalVisible() {
                  return _this3.setFileModalVisible(true);
                },
                setOutputModalVisible: function setOutputModalVisible() {
                  return _this3.setOutputModalVisible(true);
                }
              }),
              React.createElement(
                "div",
                {
                  style: {
                    flex: "1 1",
                    width: "100%",
                    height: "100%",
                    display: "flex",
                    flexDirection: "row",
                    flexWrap: "wrap"
                  }
                },
                React.createElement(SourcePalette, null),
                React.createElement(DestinationFields, null)
              )
            ),
            React.createElement(FileModal, {
              setHidden: function setHidden() {
                return _this3.setFileModalVisible(false);
              },
              visible: this.state.fileModalVisible
            }),
            React.createElement(OutputModal, {
              setHidden: function setHidden() {
                return _this3.setOutputModalVisible(false);
              },
              visible: this.state.outputModalVisible
            })
          );
        }
      }
    ]);

    return AppRoot;
  })(React.Component);

var SourcePalette =
  /*#__PURE__*/
  (function(_React$Component3) {
    _inherits(SourcePalette, _React$Component3);

    function SourcePalette(props) {
      var _this4;

      _classCallCheck(this, SourcePalette);

      _this4 = _possibleConstructorReturn(
        this,
        _getPrototypeOf(SourcePalette).call(this, props)
      );

      _this4.renderStyle();

      _this4.state = {
        filter: ""
      };
      return _this4;
    }

    _createClass(SourcePalette, [
      {
        key: "renderStyle",
        value: function renderStyle() {
          this.styles = {
            container: {
              display: "flex",
              flex: "1 2 160px",
              flexDirection: "column",
              height: "100%",
              width: 160,
              border: "1px solid " + theme.blue
            },
            body: {
              boxSizing: "border-box",
              padding: "6px 0",
              flex: "1 1 auto",
              width: "100%",
              height: 0,
              overflowY: "auto",
            },
            source: {
              //display: "inline-block",
              boxSizing: "border-box",
              float: "left",
              width: "100%",
              height: 32,
              lineHeight: "32px",
              cursor: "default",
              padding: "0 6px",
              textOverflow: 'ellipsis',
              flex: '.5, .5, auto',
            },
            searchBox: {
              height: 32,
              width: "100%",
              border: "none",
              borderBottom: "1px solid " + theme.blue,
              background: theme.background,
              color: theme.text,
              boxSizing: "border-box",
              display: "inline-block",
              fontSize: "1rem",
              fontFamily: "inherit"
            }
          };
        }
      },
      {
        key: "renderSourceElements",
        value: function renderSourceElements() {
          var _this5 = this;

          var half_width =
            arguments.length > 0 && arguments[0] !== undefined
              ? arguments[0]
              : false;
          console.log(app.state.sources.length);
          var r = [];
          var mul = 1.0;

          if (half_width) {
            mul = 0.5;
          }

          app.state.sources.forEach(function(source) {
            if (
              _this5.state.filter.length > 0 &&
              !(source.indexOf(_this5.state.filter) == 0)
            )
              return;
            var style = _this5.styles.source;
            style = Object.assign({}, style, {
              width: 100 * mul + "%",
              //maxWidth: 100 * mul + "%",
              overflow: 'hidden',
            });

            if (app.getState("selected").indexOf(source) != -1) {
              style = Object.assign({}, style, {
                background: theme.blue
              });
            } else if (false && app.getState("hovered") == source) {
              style = Object.assign({}, style, {
                boxShadow: "inset 0 0 24px -16px " + theme.blue
              });
            }

            r.push(
              React.createElement(
                "span",
                {
                  draggable: true,
                  title: source,
                  onMouseEnter: function onMouseEnter() {
                    //app.setState({hovered: source}, false, true);
                  },
                  onMouseLeave: function onMouseLeave() {
                    //app.setState({hovered: null}, false, true);
                  },
                  onMouseUp: function onMouseUp(e) {
                    if (e.ctrlKey) {
                      app.toggleSelection(source);
                      return;
                    }

                    app.setSelection(source);
                  },
                  onDragStart: function onDragStart(e) {
                    if (!app.state.selected.includes(source)) {
                      app.setSelection(source);
                    }

                    e.dataTransfer.setData(
                      "application/json",
                      JSON.stringify(app.state.selected)
                    );
                    e.dataTransfer.effectAllowed = "move";
                  },
                  style: style
                },
                source
              )
            );
          });
          return r;
        }
      },
      {
        key: "render",
        value: function render() {
          var _this6 = this;

          this.renderStyle();
          this.styles.container.size = 160;
          var half = false;

          if (
            (56 + app.getState("sources").length * 32) / window.innerHeight >
              1.0 &&
            window.innerWidth > 640
          ) {
            this.styles.container = Object.assign(this.styles.container, {
              width: 320,
              flex: "1 2 320px"
            });
            half = true;
          } else
            this.styles.container = Object.assign(this.styles.container, {
              width: 160,
              flex: "1 2 160px"
            });

          return React.createElement(
            "div",
            {
              style: this.styles.container
            },
            React.createElement(
              "div",
              {
                style: this.styles.body
              },
              React.createElement("input", {
                type: "text",
                placeholder: "filter",
                onChange: function onChange(e) {
                  return _this6.setState({
                    filter: e.target.value
                  });
                },
                value: this.state.filter,
                style: this.styles.searchBox
              }),
              this.renderSourceElements(half)
            )
          );
        }
      }
    ]);

    return SourcePalette;
  })(React.Component);

var DestinationFields =
  /*#__PURE__*/
  (function(_React$Component4) {
    _inherits(DestinationFields, _React$Component4);

    function DestinationFields(props) {
      var _this7;

      _classCallCheck(this, DestinationFields);

      _this7 = _possibleConstructorReturn(
        this,
        _getPrototypeOf(DestinationFields).call(this, props)
      );

      _this7.renderStyle();

      return _this7;
    }

    _createClass(DestinationFields, [
      {
        key: "renderStyle",
        value: function renderStyle() {
          this.styles = {
            container: {
              display: "flex",
              flex: "300 1 240px",
              flexDirection: "column",
              height: "100%",
              width: 240,
              border: "1px solid " + theme.purple
            },
            body: {
              boxSizing: "border-box",
              padding: 6,
              width: "100%",
              height: 0,
              flex: "1 1 auto",
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              flexSpacing: "start"
            },
            destination: {
              boxSizing: "border-box",
              width: 240,
              minHeight: 80,
              padding: 6,
              flex: "1 0 240px",
              background: theme.bgContrast,
              color: theme.textContrast,
              border: "1px solid " + theme.purple
            }
          };
        }
      },
      {
        key: "renderDestSources",
        value: function renderDestSources(dest) {
          var r = [];
          var sources = app.getState("destMap")[dest];
          if (!sources) return;
          var idx = 0;
          sources.forEach(function(src) {
            r.push(
              React.createElement(
                "li",
                {
                  key: JSON.stringify({
                    src: src,
                    idx: idx
                  })
                },
                React.createElement("span", null, src),
                React.createElement(
                  "button",
                  {
                    style: {
                      background: "none",
                      border: "none",
                      margin: 0,
                      color: "#f00"
                    },
                    onClick: function onClick() {
                      app.removeSources(dest, [src]);
                    }
                  },
                  "X"
                )
              )
            );
          });
          return r;
        }
      },
      {
        key: "renderDestinations",
        value: function renderDestinations() {
          var _this8 = this;

          var r = [];
          app.state.destinations.forEach(function(dest) {
            var style = _this8.styles.destination;

            if (
              false &&
              app.getState("destMap")[dest].includes(app.getState("hovered"))
            ) {
              style = Object.assign({}, style, {
                boxShadow:
                  "inset 0 0 24px -8px " +
                  theme.purple +
                  ", 0 0 16px -8px " +
                  theme.purple
              });
            }

            r.push(
              React.createElement(
                "div",
                {
                  onDragOver: function onDragOver(e) {
                    e.preventDefault();
                  },
                  onDrop: function onDrop(e) {
                    e.preventDefault();
                    app.addSources(
                      dest,
                      JSON.parse(e.dataTransfer.getData("application/json"))
                    );
                  },
                  style: style,
                  key: dest + r.length
                },
                React.createElement("b", null, dest),
                React.createElement("ul", null, _this8.renderDestSources(dest))
              )
            );
          });
          return r;
        }
      },
      {
        key: "render",
        value: function render() {
          this.renderStyle();
          return React.createElement(
            "div",
            {
              style: this.styles.container
            },
            React.createElement(
              "div",
              {
                style: {
                  width: "100%",
                  maxHeight: "100%",
                  height: "100%",
                  display: "inline-block",
                  overflowY: "scroll"
                }
              },
              React.createElement(
                "div",
                {
                  style: this.styles.body
                },
                this.renderDestinations()
              )
            )
          );
        }
      }
    ]);

    return DestinationFields;
  })(React.Component);

var FileModal =
  /*#__PURE__*/
  (function(_React$Component5) {
    _inherits(FileModal, _React$Component5);

    function FileModal(props) {
      _classCallCheck(this, FileModal);

      return _possibleConstructorReturn(
        this,
        _getPrototypeOf(FileModal).call(this, props)
      );
    }

    _createClass(FileModal, [
      {
        key: "render",
        value: function render() {
          var display = "inline-block";

          if (!this.props.visible) {
            display = "none";
          }

          return React.createElement(
            "div",
            {
              style: {
                display: display,
                position: "fixed",
                zIndex: 12,
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                background: "rgba(128, 128, 128, .5)"
              }
            },
            React.createElement(
              "div",
              {
                style: {
                  width: 640,
                  height: 400,
                  background: theme.bgContrast,
                  position: "absolute",
                  left: "50%",
                  top: "50%",
                  transform: "translate(-50%, -50%)",
                  border: "2px solid " + theme.background,
                  color: theme.textContrast,
                  padding: 12
                }
              },
              React.createElement(
                "button",
                {
                  style: {
                    position: "absolute",
                    right: 0,
                    top: 0
                  },
                  onClick: this.props.setHidden
                },
                "close"
              ),
              React.createElement("span", null, "sources: "),
              React.createElement("input", {
                type: "file",
                accept: "text",
                onChange: function onChange(e) {
                  app.loadKeyFile("sources", e);
                  e.target.value = null;
                }
                /*
        onChange={(e) =>{
          app.loadKeyFile('sources', e);
          e.target.value = null;
        }}*/
              }),
              React.createElement("span", null, app.getState("sourceFileName")),
              React.createElement("br", null),
              React.createElement("br", null),
              React.createElement("span", null, "destinations: "),
              React.createElement("input", {
                type: "file",
                accept: "text",
                onChange: function onChange(e) {
                  app.loadKeyFile("destinations", e);
                  e.target.value = null;
                }
              }),
              React.createElement(
                "span",
                null,
                app.getState("destinationFileName")
              ),
              React.createElement("br", null),
              React.createElement("br", null),
              React.createElement("span", null, "processing script: "),
              React.createElement("input", {
                type: "file",
                accept: "text/javascript",
                onChange: function onChange(e) {
                  app.loadPipeCode(e);
                  e.target.value = null;
                }
              }),
              React.createElement("span", null, app.getState("scriptFileName")),
              React.createElement("br", null),
              React.createElement("br", null),
              React.createElement("br", null),
              React.createElement("span", null, "load previous state: "),
              React.createElement("input", {
                type: "file",
                accept: "text/javascript",
                onChange: function onChange(e) {
                  app.loadSerializedState(e);
                  e.target.value = null;
                }
              })
            )
          );
        }
      }
    ]);

    return FileModal;
  })(React.Component);

var OutputModal =
  /*#__PURE__*/
  (function(_React$Component6) {
    _inherits(OutputModal, _React$Component6);

    function OutputModal(props) {
      _classCallCheck(this, OutputModal);

      return _possibleConstructorReturn(
        this,
        _getPrototypeOf(OutputModal).call(this, props)
      );
    }

    _createClass(OutputModal, [
      {
        key: "render",
        value: function render() {
          var display = "inline-block";

          if (!this.props.visible) {
            return React.createElement("div", null);
          }

          return React.createElement(
            "div",
            {
              style: {
                display: display,
                boxSizing: "border-box",
                position: "fixed",
                zIndex: 12,
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                background: "rgba(128, 128, 128, .5)",
                padding: 32
              }
            },
            React.createElement(
              "div",
              {
                style: {
                  background: theme.background,
                  position: "relative",
                  height: "100%",
                  width: "100%",
                  boxSizing: "border-box",
                  border: "2px solid " + theme.bgContrast,
                  color: theme.text,
                  padding: 12
                }
              },
              React.createElement(
                "button",
                {
                  style: {
                    position: "absolute",
                    left: 0,
                    top: 0
                  },
                  onClick: app.saveMapToText
                },
                "save output"
              ),
              React.createElement(
                "button",
                {
                  style: {
                    position: "absolute",
                    right: 0,
                    top: 0
                  },
                  onClick: this.props.setHidden
                },
                "close"
              ),
              React.createElement(
                "p",
                {
                  contentEditable: "true",
                  style: {
                    position: "absolute",
                    left: 12,
                    right: 12,
                    top: 16,
                    bottom: 0,
                    display: "inline-block",
                    boxSizing: "border-box",
                    overflowY: "scroll",
                    whiteSpace: "pre-wrap",
                    padding: 12,
                    background: theme.bgContrast,
                    color: theme.textContrast
                  }
                },
                app.mapToText() || "something went wrong"
              )
            )
          );
        }
      }
    ]);

    return OutputModal;
  })(React.Component);

function Application() {
  return React.createElement(AppRoot, null);
}

